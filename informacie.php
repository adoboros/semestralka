<?php

require "EscapeRoom.php";
require "DtbStorage.php";
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Escape rooms</title>
    <script>
        function myFunction() {
            var x = document.getElementById("navigacia");
            if (x.className === "menu") {
                x.className += " responsive";
            } else {
                x.className = "menu";
            }
        }
    </script>
    <link rel="stylesheet" href="semestralkaCSS.css">
</head>

<body>

<div class="menu" id="navigacia">
    <a href="index.php">Úvod</a>
    <a href="informacie.php" class="active">Informácie</a>
    <a href="kontakt.html">Kontakt</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <div class="toggle"></div>
        <div class="toggle"></div>
    </a>
</div>


<div class="infoBiela">
    <h1>
        Na čo slúži stránka Escape rooms?
    </h1>
    <div>
        Stránka je tvorená pre milovníkov hráčov únikových miestností na Slovensku.
    </div>
    <div>
        Nachádza sa tu databáza väčšiny slovenských "escapes".
    </div>
    <div>

    </div>
</div>

<div class="infoSeda">

    <h2>
        Unikneš zo všetkých?
    </h2>

    <div class="iconBg">
        <img src="obrazky/lock.png">
    </div>
</div>



<?php
$storage = new DtbStorage();
$storage->processData();
$storage->delete();
$storage->update();
$rooms = $storage->loadAllData();

?>
<div class="zoznamRooms">
    <table class="rooms">
        <tr>
            <th>ID</th>
            <th>Názov</th>
            <th>Minutáž</th>
            <th>Hodnotenie</th>
            <th>Mesto</th>
            <th>Kontakt</th>
        </tr>
        <?php
        foreach ($rooms as $room) {  ?>
                <tr>
                    <th style="width: 30px"><?php echo $room->getId() ?></th>
                    <th><?php echo $room->getNazov() ?></th>
                    <th><?php echo $room->getMinutaz() ?></th>
                    <th><?php echo $room->getHodnotenie() ?></th>
                    <th><?php echo $room->getMesto() ?></th>
                    <th><?php echo $room->getKontakt() ?></th>
                </tr>
        <?php } ?>
    </table>
    <div class="pridat">
        <h3 class="pridajteVlastnu">
            Pridajte svoju vlastnú Escape room
        </h3>
        <form method="post">
            <input type="text" name="nazov" placeholder="Názov" required>
            <input type="number" step="0.1" name="hodnotenie" placeholder="Hodnotenie" required>
            <input type="tel" name="kontakt" placeholder="+421.." required>
            <input type="text" name="psc" placeholder="PSČ" required>
            <input type="text" name="ulica" placeholder="Ulica" required>
            <input type="text" name="mesto" placeholder="Mesto" required>
            <input type="text" name="minutaz" id="minutaz" placeholder="Minutáž" required>
            <input type="submit" value="Pridať" name="sent">
        </form>
    </div>

    <div class="odstranit">
        <h3 class="odstranteNeexistujucu">
            Odstráňte neexistujúcu
        </h3>
        <form method="post">
            <input type="number" min="1" id="mazanieID" name="zmazat" placeholder="ID" required>
            <input type="submit" value="Zmazať" name="delete">
        </form>
    </div>

    <div class="zmenit">
        <h3 class="zmenteNeaktualnu">
            Zmeňte neaktuálnu
        </h3>
        <form method="post">
            <label for="stlpec">Zvoľte menenú hodnotu:</label>
            <select name="stlpec" id="stlpec">
                <option value="nazov">Názov</option>
                <option value="minutaz">Minutáž</option>
                <option value="hodnotenie">Hodnotenie</option>
                <option value="mesto">Mesto</option>
                <option value="kontakt">Kontakt</option>
            </select>
            <input type="text" name="menene" placeholder="Nová hodnota" required>
            <input type="number" min="1" name="zaznam" placeholder="ID" required>
            <input type="submit" value="zmeniť" name="zmenit">
        </form>
    </div>
</div>



</body>
</html>
